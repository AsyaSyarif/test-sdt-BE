<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DevicesValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string|max:50',
            'connectivity'  => 'required|string',
            'ip_address'    => 'required|string',
            'mac_address'   => 'required|string',
            'user_id'       => 'required|string',
            'is_active'     => 'required',
        ];
    }
}
