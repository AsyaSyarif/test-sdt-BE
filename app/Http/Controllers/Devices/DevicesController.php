<?php

namespace App\Http\Controllers\Devices;

use App\Http\Controllers\Controller;
use App\Models\Devices;
use App\Repositories\Devices\DeviceRepository;
use App\Http\Requests\DevicesValidationRequest;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Validator;

class DevicesController extends Controller
{
    use ApiResponser;
    protected $model;

    public function __construct(Devices $device)
    {
        $this->model = New DeviceRepository($device);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            $device = $this->model->GetAllDevices();
            return $this->successResponse($device);

        } catch (\Throwable $th) {
            return $this->badRequestResponse("Something went wrong!");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DevicesValidationRequest $request)
    {
        try {
           $this->model->CreateDevice($request->validated());
           return $this->createdResponse();
        } catch (\Throwable $th) {
            return $this->badRequestResponse("Something went wrong!");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $device = $this->model->DeviceById($id);
            return $this->successResponse($device);
        } catch (\Throwable $th) {
            return $this->notFoundResponse();
        }

    }

    public function DeviceByUserID($user_id){
        try {
            $device = $this->model->DeviceByUserID($user_id);
            return $this->successResponse($device);
        } catch (\Throwable $th) {
            $this->notFoundResponse();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DevicesValidationRequest $request, $id)
    {
        try {
            $this->model->UpdateDevice($id, $request->validated());
            return $this->successUpdatedResponse();
         } catch (\Throwable $th) {
             return $this->badRequestResponse("Something went wrong!");
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->model->DeleteDevice($id);
            return $this->successDeletedResponse();
        } catch (\Throwable $th) {
            $this->notFoundResponse();
        }
    }


}
