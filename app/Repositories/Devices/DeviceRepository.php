<?php

namespace App\Repositories\Devices;

use App\interfaces\Devices\DevicesInterfaces;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class DeviceRepository implements DevicesInterfaces {

  protected $_device;
  public function __construct(Model $model)
  {
    $this->_device = $model;
  }

  public function GetAllDevices()
  {
    return $this->_device->all();
  }

  public function DeviceById($device_id)
  {
    return $this->_device->findOrFail($device_id);
  }

  public function DeviceByUserId($user_id)
  {
    return $this->_device->where('user_id', $user_id)->get();
  }

  public function CreateDevice($request)
  {
    $this->_device->create([
      'name'          =>  $request['name'],
      'connectivity'  =>  $request['connectivity'],
      'ip_address'    =>  $request['ip_address'],
      'mac_address'   =>  $request['mac_address'],
      'is_active'     =>  $request['is_active'],
      'user_id'       =>  $request['user_id']
    ]);
  }

  public function UpdateDevice($device_id, $request)
  {
    $device = $this->DeviceById($device_id);
    $device->name         = $request['name'];
    $device->connectivity = $request['connectivity'];
    $device->ip_address   = $request['ip_address'];
    $device->mac_address  = $request['mac_address'];
    $device->is_active    = $request['is_active'];
    $device->user_id      = $request['user_id'];
    $device->save();
  }

  public function DeleteDevice($device_id)
  {
    $this->DeviceById($device_id)->delete();
  }
}