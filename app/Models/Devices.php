<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Devices extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'connectivity',
        'ip_address',
        'mac_address',
        'is_active',
        'user_id'
    ];

    protected $hidden = [
        'deleted_at'
    ];

    protected $casts = [
        'is_active'   => 'boolean'
    ];
}
