<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

trait ApiResponser {

  var $defaultCreated = "Data created successfully";
  var $defaultSuccessRetrieved = "Data retrieved successfully";
  var $defaultSuccessUpdated = "Data updated successfully";
  var $defaultSuccessDeleted = "Data deleted successfully";
  var $defaultNotFound = "Data not found!";

  public function successResponse($data = [], $message = "", $isSuccess = true, int $statusCode = Response::HTTP_OK): JsonResponse
  {
      $dataResponse = [
          'message'       => $message ? $message : $this->defaultSuccessRetrieved,
          'statusCode'    => $statusCode,
          'success'       => $isSuccess,
          'data'          => $data
      ];
      return new JsonResponse($dataResponse, $statusCode);
  }

  public function successUpdatedResponse($data = [], $message = "", $isSuccess = true, int $statusCode = Response::HTTP_OK): JsonResponse
  {
      $dataResponse = [
          'message'       => $message ? $message : $this->defaultSuccessUpdated,
          'statusCode'    => $statusCode,
          'success'       => $isSuccess,
          'data'          => $data
      ];
      return new JsonResponse($dataResponse, $statusCode);
  }

  public function successDeletedResponse($data = [], $message = "", $isSuccess = true, int $statusCode = Response::HTTP_OK): JsonResponse
  {
      $dataResponse = [
          'message'       => $message ? $message : $this->defaultSuccessDeleted,
          'statusCode'    => $statusCode,
          'success'       => $isSuccess,
          'data'          => $data
      ];
      return new JsonResponse($dataResponse, $statusCode);
  }

  public function createdResponse($message = "", $isSuccess = true, $data = [], $statusCode = Response::HTTP_CREATED): JsonResponse
  {

      $data = [
          'message'       => $message ? $message : $this->defaultCreated,
          'statusCode'    => 201,
          'success'       => $isSuccess,
          'data'          => $data
      ];

      return new JsonResponse($data, $statusCode);
  }

  public function notFoundResponse($data = [], string $message = '', $statusCode = Response::HTTP_NOT_FOUND ): JsonResponse
  {
      $dataResponse = [
          'message'       => $message ? $message : $this->defaultNotFound,
          'statusCode'    => $statusCode,
          'success'       => true,
          'data'          => $data
      ];

      return new JsonResponse($dataResponse, $statusCode);
  }

  public function badRequestResponse(string $message = "" ,$statusCode = Response::HTTP_BAD_REQUEST): JsonResponse
  {
    $dataResponse = [
        'message'       => $message ? $message : $this->defaultNotFound,
        'statusCode'    => $statusCode,
        'success'       => true,
        'data'          => []
    ];

    return new JsonResponse($dataResponse, $statusCode);
  }

}