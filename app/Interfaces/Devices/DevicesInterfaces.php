<?php

namespace App\interfaces\Devices;

use Illuminate\Database\Eloquent\Collection;

interface DevicesInterfaces {
  public function GetAllDevices();
  public function DeviceById($device_id);
  public function DeviceByUserId($user_id);
  public function CreateDevice($request);
  public function UpdateDevice($device_id, $request);
  public function DeleteDevice($device_id);
}