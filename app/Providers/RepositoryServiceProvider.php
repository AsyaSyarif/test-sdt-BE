<?php

namespace App\Providers;

use App\interfaces\Devices\DevicesInterfaces;
use App\Repositories\Devices\DeviceRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            DevicesInterfaces::class,
            DeviceRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
